Supersoft CPU Diagnostics
=========================

`CPUTEST.COM` is Diagnostics II, version 1.2, CPU test by Supersoft
Associates. No source appears to be available; this binary came from [a
GitHub repo][com]. It tests both 8080 and Z80 CPUs.

Contents:
- [Usage Notes](#usage-notes)
- [Disassembly Notes](#disassembly-notes)
- [Building](#building)


Usage Notes
-----------

On failure of an opcode test, a message like the following will be printed:

    CPU FAILED:
    ERROR COUNT 0001H
    INSTRUCTION SEQUENCE WAS 050000H
    REGISTER f CONTAINS 46H
    BUT SHOULD CONTAIN 56H
    REGISTER VALUE BEFORE INSTRUCTION SEQUENCE WAS 02H
    TEST NUMBER  0011H

The instruction sequence above gives the opcode and operands that caused
the failure. For 1- and 2-byte instructions the remaining bytes are padded
with NOP ($00) opcodes. In the above example, the failing instruction was
`$05`, or `DCR B` / `dec b`.

All registers are loaded with test data before executing the instruction
under test, and all are checked after, but only one failure appears to be
shown. In the case above, it's the flags register which had/expected the
following values. (This test was run on an 8080, so note that bit 2 of F is
always `1` when pushed on the stack, represented by `=` below, thus the
initial value of $02 rather than $00.)

    Before Test     F=$02   sz-h-p=c
    Expected Result F=$56   sZ-H-P=c
    Actual Result   F=$46   sZ-h-P=c

From this we can see that the fault detected was that the `DCR m`/`dec r`
instruction is not correctly assigning the auxiliary carry / half carry
flag.

However, on occasion the error messages can be incorrect. Here's an example
(on a broken simulator) of an obviously bad error message that actually
should have been a failure on the flags register:

    INSTRUCTION SEQUENCE WAS 030000H
    REGISTER c CONTAINS 19H
    BUT SHOULD CONTAIN 19H
    REGISTER VALUE BEFORE INSTRUCTION SEQUENCE WAS 18H
    TEST NUMBER  0005H


Disassembly Notes
-----------------

This binary essentially contains two copies of a test program for both
8080/8085 and Z80, with substantial duplication between them. (Presumably
the two copies are not _entirely_ identical, or there would be no point to
this.)

Dozens of routines are identical copies except for using different storage
locations. Where these have been identified, the routines have the same
name distinguished by a Greek letter suffix. In many cases only one of the
two duplicate routines is commented, and a single name (with or without a
suffix) should not be taken to indicate that a duplicate routine does not
exist.

At the very start of the program a test is done to see if it's running on
an 8080 or Z80 processor, and based on this it jumps to one of two
nearly-identical routines: `testZ80` or `test8080`. These two versions of
the code still themsleves test for Z80 vs. 8080 and configure themselves
with appropriate settings to take the right branches on their many 8080 vs.
Z80 conditionals.


Building
--------

The repo's top-level build script in [`../Build`](../Build) (relative to
this directory) runs the disassembly and reassembly and compares the
reassembly results to the original binary.

`CPUTEST.COM` is the original binary.

#### Disassembly

The `disassembly/` subdirectory contains a disassembly done with `z80dasm`
and the associated annotation files defining code vs. data areas and a few
symbols. (These symbols are not necessarily the same as those in the
reverse-engineered version below).

`z80dasm` is available in most Linux distributions but not for Windows. The
build script will automatically skip the disassembly stage on Windows
systems by detecting MINGW.

#### Source Code

In this directory `cputest.z80` is the reverse-engineered source code, and
is assembled with `asl`, the [Macroassembler AS][asl]. The listing from the
assembly is committed to `cputest.lst` for the convenience of those who
need addresses and data to reference when debugging the `.COM` file.

If the build script finds `asl` is found in your path, that will be used,
otherwise it will look in a few obvious places for it. (See the build
script for details.)

The easiest way to get Macroassembler AS on Linux is to build it from the
[`asl-releases`] repo on GitHub. On Windows, just download the Win32 binary
release from the [ASL downloads page][asl-dl] and unpack it under
`C:\Program Files (x86)\asl\`.

To see a diff of errors when comparing generated ROM binaries to the
original you will also need [`meld`][]; this is also available in most
Linux distributions, and downloadable for Windows.

The [ASL manual][aslman] is available online.



<!-------------------------------------------------------------------->
[com]: https://github.com/JALsnipe/i8080-core/blob/master/CPUTEST.COM

<!-- Building -->
[`asl-releases`]: https://github.com/Macroassembler-AS/asl-releases
[`meld`]: https://meldmerge.org/
[asl-dl]: http://john.ccac.rwth-aachen.de:8000/as/download.html
[asl]: http://john.ccac.rwth-aachen.de:8000/as/
[aslman]: http://john.ccac.rwth-aachen.de:8000/as/as_EN.html
