####################################################################
#   Keep labels ???8 chars from shifting the instruction field over
#   XXX this should go in the build files
/^([A-Za-z0-9_]{8,15})\t\t*/s//\1/

####################################################################
#   Since we're displaying ASCII data after the addresses, the
#   character displayed can be a backslash, which the assembler
#   will misinterpret as a line continuation unless we put a
#   character after it. Unfortunately we can't seem to put a space
#   after it, so  instead we put some printable stuff.
#   XXX This should go in the build files
s/\\ $/\\ (backslash)/g

####################################################################
#   Generic changes

s/ defw / dw   /
s/ defb / db   /
