cpmsrc: Sources for the CP/M Operating System
=============================================

The [Digital Research Source Code][drsc] page on cpm.z80.de has extensive
sources, not all from Digital Research. References to [[drsc]] below
indicate that the source was downloaded from that page; this is typically
just to make on-line reading easier.

- `2.2orig/`: Original CP/M 2.2 assembly and PL/M source code, extracted
  from the [`cpm2-plm.zip`] file on [[dsrc]].



<!-------------------------------------------------------------------->
[`cpm2-plm.zip`]: http://www.cpm.z80.de/download/cpm2-plm.zip
[drsc]: http://www.cpm.z80.de/source.html
